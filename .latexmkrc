$latex  = 'platex -src-specials -halt-on-error -file-line-error -interaction=nonstopmode';
$bibtex = 'jbibtex';
$dvipdf = "dvipdfmx %O -o %D %S";
$dvips  = 'pdvips';
