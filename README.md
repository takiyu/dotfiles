**個人的環境導入リポジトリ**

注意）githubが最新版
*<導入方法>*  
cd ~/  
git clone https://bitbucket.org/takiyu/dotfiles.git  
cd dotfiles  
./init.sh  

以上のコマンドを実行すると、ホームディレクトリにシンボリックリンクが貼られて環境が導入されます。